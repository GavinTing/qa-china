# 实现rnn+attention+beamsearch的中文闲聊机器人

##结构
seq2seq
![seq2seq](./image/32a87cf8d0353ceb0037776f833b92a7.jpg)

attention
![attention](./image/global_attn.png)

一些细节：
* pack_pad的使用
* 梯度裁剪的使用
* checkpoint
* 内存的优化。append(loss)会占用显存/内存
 


### 参考
https://github.com/ywk991112/pytorch-chatbot
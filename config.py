import torch

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

#是否加载模型
is_load_model = True
#模型加载地址
loadFilename = './checkpoints/3_model.tar'

#文件路径：
data_path = './data/xiaohuangji_toy.tsv'

#轮转次数
iter_num = 10
batch_size = 20

#embedding:
embedding_size = 200

#encoder:
encoder_hidden_dim = 512
encoder_learnrate = 0.0001

#attention
atten_dot = 'dot'

#decoder:
decoder_hidden_dim = 512
decoder_learnrete = 0.0001*5

teacher_forcing_ratio = 0.5




#author: ding xg
from model import *
from data_process import get_data_iter
from torch.nn import Embedding
import config as cog

def load_model(path):

    _, TEXT, _ = get_data_iter()
    #设置embeeding  encoder 和 decoder 共享embedding
    embedding = Embedding(len(TEXT.vocab),cog.embedding_size)
    #定义模型类
    encoder = Encoder(embedding=embedding,embedding_dim=cog.embedding_size,
                      hidden_dim=cog.encoder_hidden_dim)
    decoder = Decoder(att_mode=cog.atten_dot,embedding=embedding,
                      embedding_dim=cog.embedding_size,hidden_dim=cog.decoder_hidden_dim,
                      output_size=len(TEXT.vocab))

    print('模型加载********')
    loadFilename = path
    checkpoint = torch.load(loadFilename)
    encoder.load_state_dict(checkpoint['encoder'])
    decoder.load_state_dict(checkpoint['decoder'])
    print('加载完成')
    encoder = encoder.to(cog.device)
    decoder = decoder.to(cog.device)

    #对dropout batch_norm起作用，即使用这两种的一定要加 trian(false)
    encoder.train(False)
    decoder.train(False)

    return encoder,decoder,TEXT
    pass

#序列化句子  sequence,bsz,word
def sentence2tensor(s,TEXT):
    # print(s)
    # a = TEXT.pad([s,s])
    # print(TEXT.numericalize(a))
    # print(TEXT.numericalize(s))
    # print(TEXT.pad(TEXT.numericalize(s)))
    # print(TEXT.reverse(TEXT.numericalize(s)))
    s_pad = TEXT.pad([s])#填充
    s_numer = TEXT.numericalize(s_pad)#序列化
    return s_numer
    pass


# def addTopk(topi, topv, decoder_hidden, beam_size, TEXT):
#     topv = torch.log(topv)
#     terminates, sentences = [], []
#     for i in range(beam_size):
#         if topi[0][i] == TEXT.vocab.stoi['<eos>']:
#             terminates.append(([voc.index2word[idx.item()] for idx in self.sentence_idxes] + ['<eos>'],
#                                self.avgScore())) # tuple(word_list, score_float
#             continue
#         idxes = self.sentence_idxes[:] # pass by value
#         scores = self.sentence_scores[:] # pass by value
#         idxes.append(topi[0][i])
#         scores.append(topv[0][i])
#         print(sentences)
#         # sentences.append(Sentence(decoder_hidden, topi[0][i], idxes, scores))
#     return terminates, sentences

def beam_search(decoder, decoder_hidden, encoder_outputs,TEXT,input_var,beam_size,max_target_len):
    target_var = input_var
    decoder_input = target_var[0].unsqueeze(0)
    for t in range(1, max_target_len):
        decoder_output, decoder_hidden, decoder_attn = decoder(
            decoder_input, decoder_hidden, encoder_outputs
        )
        topv, topi = decoder_output.topk(beam_size)
        # addTopk(topi, topv, decoder_hidden, TEXT=TEXT, beam_size=beam_size)
        print(topv)
        print(topi)
    pass

    pass


#评估
def evaluate(s,path):
    torch.set_grad_enabled(False)

    encoder,decoder,TEXT = load_model(path)
    input_var = sentence2tensor(s,TEXT).to(cog.device)

    encoder_outputs, encoder_hidden = encoder(input_var)
    decoder_hidden = encoder_hidden[:decoder.n_layers]

    max_target_len = input_var.size(0)
    a = beam_search(decoder, decoder_hidden, encoder_outputs,TEXT,input_var=input_var,beam_size=2,max_target_len=max_target_len)
    print(a)
    # print(input_var)

    pass


if __name__ == '__main__':
    # encoder,decoder,TEXT = load_model('./checkpoints/3_model.tar')
    # s = '这是我测试的一句话：'
    # sentence2tensor(s,TEXT)

    s = '这是我测试的一句话：'
    evaluate(s,'./checkpoints/3_model.tar')

    pass
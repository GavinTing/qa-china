#author: ding xg
from data_process import get_data_iter
from torch.nn.modules import Embedding
import config as cog
from model import Encoder,Decoder
from torch import optim
from tqdm import tqdm
import torch
import random
import torch.nn.functional as F
import os


if __name__ == '__main__':

    #获取数据
    train_iter, TEXT, data = get_data_iter()

    #设置embeeding  encoder 和 decoder 共享embedding
    embedding = Embedding(len(TEXT.vocab),cog.embedding_size)

    #定义模型类
    encoder = Encoder(embedding=embedding,embedding_dim=cog.embedding_size,
                      hidden_dim=cog.encoder_hidden_dim)
    decoder = Decoder(att_mode=cog.atten_dot,embedding=embedding,
                      embedding_dim=cog.embedding_size,hidden_dim=cog.decoder_hidden_dim,
                      output_size=len(TEXT.vocab))
    #优化器
    encoder_optimer = optim.Adam(encoder.parameters(),lr=cog.encoder_learnrate)
    decoder_optimer = optim.Adam(decoder.parameters(),lr=cog.decoder_learnrete)
    start_iter = 0
    #模型加载
    if cog.is_load_model:
        print('模型加载********')
        loadFilename = cog.loadFilename
        checkpoint = torch.load(loadFilename)
        encoder.load_state_dict(checkpoint['encoder'])
        decoder.load_state_dict(checkpoint['decoder'])
        encoder_optimer.load_state_dict(checkpoint['encoder_opt'])
        decoder_optimer.load_state_dict(checkpoint['decoder_opt'])
        start_iter = checkpoint['iteration']+1
        print('加载完成')
        pass
    encoder = encoder.to(cog.device)
    decoder = decoder.to(cog.device)


    # 单个epoch的loss
    loss_iter = []
    for iteration  in tqdm(range(start_iter,cog.iter_num)):
        #一个epoch中所有batch的loss
        loss_item = []
        for batch in train_iter:
            #长度
            max_target_len = batch.output.size(0)
            batch_size = batch.output.size(1)

            #梯度清零
            encoder_optimer.zero_grad()
            decoder_optimer.zero_grad()

            #获取变量
            input_var = batch.input.to(cog.device)
            target_var = batch.output.to(cog.device)


            #(seqence,bsz,hidden_size)
            encoder_outputs, encoder_hidden = encoder(input_var)
            decoder_hidden = encoder_hidden[:decoder.n_layers]

            use_teacher_forcing = True if random.random() < cog.teacher_forcing_ratio else False
            decoder_input = target_var[0].unsqueeze(0)

            loss = 0
            if use_teacher_forcing:
                for t in range(1,max_target_len):
                    decoder_output, decoder_hidden, decoder_attn = decoder(
                        decoder_input, decoder_hidden, encoder_outputs
                    )
                    decoder_input = target_var[t].unsqueeze(0)  # Next input is current target
                    loss += F.cross_entropy(decoder_output, target_var[t], ignore_index=TEXT.vocab.stoi['<eos>'])
                pass
            else:
                for t in range(1,max_target_len):
                    decoder_output, decoder_hidden, decoder_attn = decoder(
                        decoder_input, decoder_hidden, encoder_outputs
                    )
                    _, topi = decoder_output.topk(1)  # [64, 1]

                    decoder_input = torch.LongTensor([[topi[i][0] for i in range(batch_size)]])
                    decoder_input = decoder_input.to(cog.device)
                    loss += F.cross_entropy(decoder_output, target_var[t], ignore_index=TEXT.vocab.stoi['<eos>'])
                pass
            loss.backward()
            clip = 50.0
            _ = torch.nn.utils.clip_grad_norm_(encoder.parameters(), clip)
            _ = torch.nn.utils.clip_grad_norm_(decoder.parameters(), clip)
            encoder_optimer.step()
            decoder_optimer.step()
            loss_item.append(float(loss))
            print('batch_loss')
            print(loss)
            pass
        print('******iter_loss******')
        loss_iter.append(loss_item)
        print(sum(loss_item)/len(train_iter))
        current_loss = sum(loss_item)/len(train_iter)


        #模型保存  #重复性代码，后期考虑写的具有扩展性
        torch.save({
            'iteration':iteration,
            'encoder':encoder.state_dict(),
            'decoder':decoder.state_dict(),
            'encoder_opt':encoder_optimer.state_dict(),
            'decoder_opt':decoder_optimer.state_dict(),
            'current_loss': current_loss,
            'loss_iter':loss_iter,
        },os.path.join('checkpoints','{}_{}.tar'.format(iteration,'model')))
        print(str(iteration),'_epoch 保存完成')

        pass



    pass
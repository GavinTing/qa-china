#autho:ding xg
import torch
from torch.nn import Module
from torch import nn
from torch.nn import functional as F



"""
模型文件
#参考文献：
https://github.com/ywk991112/pytorch-chatbot
"""


"编码器"
class Encoder(Module):
    def __init__(self,embedding,embedding_dim,hidden_dim,num_layers = 1,dropout=0):
        super(Encoder,self).__init__()
        # self.vocab_size = vocab_size
        # self.embedding = embedding_dim
        # self.hideen_dim = hidden_dim
        # self.embedding = nn.Embedding(vocab_size,embedding_dim)
        self.embedding = embedding
        self.embedding_dim = embedding_dim
        self.hideen_dim = hidden_dim
        self.num_layers = num_layers
        self.GRU = nn.GRU(embedding_dim,hidden_dim,num_layers=1,bidirectional=True)
        pass

    #缺少了pack_pad程序，后期补上。
    def forward(self,input,hidden=None):
        seq_length,batch_size = input.size()
        if hidden is None:
            h_0 = input.data.new(self.num_layers,batch_size,self.hideen_dim)
            pass
        else:
            h_0 = hidden
            pass
        embedded = self.embedding(input)
        outputs,hidden = self.GRU(embedded)
        outputs = outputs[:,:,:self.hideen_dim] + outputs[:,:,self.hideen_dim:]

        return outputs,hidden
        pass
    pass

"""
注意力机制
word by word
支持输入一个单词，而不是一个序列。注意要unsqueeze
"""
class Atten(Module):
    def __init__(self, method, hidden_size):
        super(Atten, self).__init__()
        self.method = method
        if self.method not in ['dot', 'general', 'concat']:
            raise ValueError(self.method, "is not an appropriate attention method.")
        self.hidden_size = hidden_size
        if self.method == 'general':
            self.attn = torch.nn.Linear(self.hidden_size, hidden_size)
        elif self.method == 'concat':
            self.attn = torch.nn.Linear(self.hidden_size * 2, hidden_size)
            self.v = torch.nn.Parameter(torch.FloatTensor(hidden_size))

    def dot_score(self, hidden, encoder_output):
        return torch.sum(hidden * encoder_output, dim=2)

    def general_score(self, hidden, encoder_output):
        energy = self.attn(encoder_output)
        return torch.sum(hidden * energy, dim=2)

    def concat_score(self, hidden, encoder_output):
        energy = self.attn(torch.cat((hidden.expand(encoder_output.size(0), -1, -1), encoder_output), 2)).tanh()
        return torch.sum(self.v * energy, dim=2)

    def forward(self, hidden, encoder_outputs):
        # Calculate the attention weights (energies) based on the given method
        if self.method == 'general':
            attn_energies = self.general_score(hidden, encoder_outputs)
        elif self.method == 'concat':
            attn_energies = self.concat_score(hidden, encoder_outputs)
        elif self.method == 'dot':
            attn_energies = self.dot_score(hidden, encoder_outputs)

        # Transpose max_length and batch_size dimensions
        attn_energies = attn_energies.t()

        # Return the softmax normalized probability scores (with added dimension)
        return F.softmax(attn_energies, dim=1).unsqueeze(1)

    pass

"""
解码器
"""
class Decoder(Module):
    def __init__(self,embedding,embedding_dim,hidden_dim,output_size,att_mode,n_layers=1,dropout=0.2):
        super(Decoder,self).__init__()
        self.embedding = embedding
        self.embedding_dropout = nn.Dropout(dropout)
        self.gru = nn.GRU(embedding_dim,hidden_dim,n_layers,dropout=0)
        self.concat = nn.Linear(hidden_dim*2,hidden_dim)
        self.out = nn.Linear(hidden_dim,output_size)
        self.n_layers = n_layers

        self.attn = Atten(att_mode,hidden_dim)

        pass
    def forward(self, input_step,last_hidden,encoder_outputs):

        embedded = self.embedding(input_step)
        embedded = self.embedding_dropout(embedded)
        rnn_output, hidden = self.gru(embedded, last_hidden)
        attn_weights = self.attn(rnn_output, encoder_outputs)
        context = attn_weights.bmm(encoder_outputs.transpose(0, 1))

        rnn_output = rnn_output.squeeze(0)
        context = context.squeeze(1)

        concat_input = torch.cat((rnn_output, context), 1)
        concat_output = torch.tanh(self.concat(concat_input))
        output = self.out(concat_output)
        output = F.softmax(output, dim=1)
        return output, hidden,attn_weights
        pass

    pass

if __name__ == '__main__':

    pass
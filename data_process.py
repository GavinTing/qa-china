#author:ding xg
import pandas as pd
import torchtext
from torchtext.data import Field
import jieba
import config as cog

def get_data_iter():
    #读取数据
    TEXT = torchtext.data.ReversibleField(sequential=True,use_vocab=True,init_token='<sos>',eos_token='<eos>',tokenize=jieba.lcut,include_lengths=False)
    data = torchtext.data.TabularDataset(path=cog.data_path,format='tsv',fields=[('input',TEXT),('output',TEXT)],skip_header=True)
    TEXT.build_vocab(data)
    train_iter = torchtext.data.Iterator(dataset=data,batch_size=cog.batch_size,shuffle=True)
    return train_iter,TEXT,data
    pass



if __name__ == '__main__':
    train_iter, TEXT, data = get_data_iter()
    print(len(TEXT.vocab))
    print(TEXT.vocab.itos)

    print(len(TEXT.vocab.itos))

    pass